set(MESSAGESERVICEPLUGINS_DIR ${QMF_PLUGINS_DIR}/messageservices)

add_subdirectory(imap)
add_subdirectory(pop)
add_subdirectory(qmfsettings)
add_subdirectory(smtp)
